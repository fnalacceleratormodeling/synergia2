#!/usr/bin/env python

import synergia

r = synergia.simulation.Resume()
# these arguments will tell Resume to use the original options
r.propagate(False, -1, False, -1)
